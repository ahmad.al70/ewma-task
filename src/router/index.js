import { createWebHistory, createRouter } from "vue-router";
import MainLayout from '../components/MainLayout.vue'
import MovieDetails from '../components/movies/MovieDetails.vue'
import ListOfMovies from '../components/movies/ListOfMovies.vue'



const routes = [
  {
    path: "/",
    name: "main-layout",
    component: MainLayout,
    children:[
        {
            path: '/',
            name: 'list-of-movies',
            component:  ListOfMovies
          },
          {
            path: '/movies/:movie_id/details',
            name: 'movie-details',
            component: MovieDetails
          }
    ]
  },
  
]

const router = createRouter({
    history: createWebHistory(),
    routes,
  });
  

export default router