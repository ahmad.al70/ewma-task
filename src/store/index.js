import { createStore } from "vuex";

export default createStore({
  state: {
    query:"",
    startReleaseDate:null,
    endReleaseDate:null,
    loading:false
    
  },
  mutations: {
    setQuery(state, query) {
      state.query=query
    },
    setLoadingStatus(state, loading) {
      state.loading=loading
    },
    setReleaseDate(state, rangDate) {
        state.startReleaseDate=rangDate[0]
        state.endReleaseDate=rangDate[1]
      },
  
  }
});