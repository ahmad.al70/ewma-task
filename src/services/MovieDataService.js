import http from "../http-common";
const apiKey='f62f750b70a8ef11dad44670cfb6aa57'
class MovieDataService {
  getAll(query) {
    return http.get(`/discover/movie?api_key=${apiKey}${query}`);
  }
  getAllGenere() {
    return http.get(`/genre/movie/list?api_key=${apiKey}&language=en-US`);
  }
  getMovieDetails(movie_id) {
    return http.get(`/movie/${movie_id}?api_key=${apiKey}&language=en-US`);
  }

  getMovieCredites(movie_id) {
    return http.get(`/movie/${movie_id}/credits?api_key=${apiKey}&language=en-US`);
  }

}

export default new MovieDataService();
